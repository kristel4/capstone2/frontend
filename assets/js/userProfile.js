let baseUrl = 'http://localhost:4000/api'; //for shorter code of URLs
let avatarInput = document.querySelector('#avatarInput');
let profileImage = document.querySelector("#avatar");
let token = localStorage.getItem('token');
let userId = localStorage.getItem('id');
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let email = document.querySelector('#email');
let mobile = document.querySelector("#mobileNumber");
let courseTable = document.querySelector("#courseTable");
let uploadBtn = document.querySelector('#uploadBtn');


avatarInput.addEventListener('change', (e) => {
	// e.preventDefault();
	let file = e.target.files[0];
	let reader = new FileReader();


	reader.onload = (e) => {
		profileImage.src = e.target.result
		console.log(e)
	}

	reader.readAsDataURL(file);
})

uploadBtn.addEventListener('click', (e) => {
	e.preventDefault();
	uploadImage(profileImage.src)
})


const uploadImage = (avatar) => {
	fetch(`${baseUrl}/users/upload`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			userId,
			avatar
		})		
	}).then( res => {
		//check if there is an error encountered
		if(res.status !== 200) {
			//if so return the error text
				return res.statusText
		} 
		return res.json()
	})
	.then(data => {
		console.log(data);

		if(data === true) {
			alert('Avatar successfully uploaded!');
		} 
		else {
			alert(data)
		} 
	})
}

const getUserProfile = () => {

	fetch(`${baseUrl}/users/details`, {
		methods: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		let courses = data.enrollments;
		firstName.innerHTML = data.firstName;
		lastName.innerHTML = data.lastName;
		email.innerHTML = data.email;
		mobile.innerHTML = data.mobileNo;
		profileImage.src = data.avatar;

		let i;
		for(i = 0; i < courses.length; i++) {
			let enrolledOn = courses[i].enrolledOn;
			fetch(`${baseUrl}/courses/${courses[i].courseId}`, {
				methods: 'GET',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`					
				}
			})
			.then( res => res.json())
			.then(data => {
				let newRow = document.createElement('tr');
				let enrolledTd = newRow.appendChild(document.createElement('td'));
				let courseTd = newRow.appendChild(document.createElement('td'));
				let descriptionTd = newRow.appendChild(document.createElement('td'));
				let priceTd = newRow.appendChild(document.createElement('td'));

				
				enrolledTd.innerHTML = enrolledOn;
				courseTd.innerHTML = data.name;
				descriptionTd.innerHTML = data.description;
				priceTd.innerHTML = `PHP ${data.price}`
				courseTable.appendChild(newRow);
				console.log(data);

			})
		}
		// fetch(`${baseUrl}/courses/:${courses.courseId}`)
	})
}


getUserProfile()


