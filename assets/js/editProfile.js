let baseUrl = 'http://localhost:4000/api'; //for shorter code of URL's
let token = localStorage.getItem('token');
let userId = localStorage.getItem('id');
let editProfile = document.querySelector("#editProfile")
let changePassword = document.querySelector("#changePassword")
let adminUser = localStorage.getItem("isAdmin");


const getUserProfile = () => {

	fetch(`${baseUrl}/users/details`, {
		methods: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		firstName.value = data.firstName
		lastName.value = data.lastName
		userEmail.value = data.email
		mobileNo.value = data.mobileNo
		})
	
}
//end of getting data
getUserProfile()
//updating userprofile
editProfile.addEventListener('submit', (e) => {
	e.preventDefault()

	let firstName = document.querySelector('#firstName').value;
	let lastName = document.querySelector('#lastName').value;
	let mobileNumber = document.querySelector('#mobileNo').value;
	let email = document.querySelector('#userEmail').value;

	fetch(`${baseUrl}/users/updating-details`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			userId,
			firstName: firstName,
			lastName: lastName,
			mobileNo: mobileNumber,
			email: email
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//redirect to course
				alert('Profile successfully updated!')
			} else {
				//redirect in creating course
				alert("Something went wrong!")
			}
		})
})

// //updating password
	
changePassword.addEventListener('submit', (e) => {
	e.preventDefault()

	let password1 = document.querySelector("#password1").value;
	let password2 = document.querySelector("#password2").value;

	if((password1 !== '' && password2 !== '' ) && (password2 === password1)){

		fetch(`${baseUrl}/users/change-password`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				password: password1,
				userId: localStorage.getItem('id')
				})
			})
			.then(res => {
				return res.json()
			})
			.then(data => {
				if(data === true){
					//redirect to course
					alert('Password changed!')
				} else {
					//redirect in creating course
					alert("Something went wrong!")
				}
			})
	} else {
		alert("Please check the information provided!")
	}
})
