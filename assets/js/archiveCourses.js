let token = localStorage.getItem('token')
let coursesContainer = document.querySelector('#coursesContainer');
let fetching = document.querySelector('#fetching');


const getAllCourses = () => {
	fetch ('http://localhost:4000/api/courses/archives', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/jason',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)

		let i;
		for(i = 0; i < data.length; i++) {

			let container = document.createElement('div');

			container.className = 'col-6';

			let card = document.createElement('div');
			card.className = 'card p-3'
			card.innerHTML = `

				<div class="mb-3">
					<div>
					    <h5 class="card-title text-center">${data[i].name}</h5>
					    <p class="card-text text-left">${data[i].description}<p>
					    <p class="card-text text-right">${data[i].price}<p>
					</div>
					<div class="card-footer">
						<a href="./activateCourse.html?courseId=${data[i]._id}" value="{course._id}" class="btn text-white btn-block activateButton">Activate</a>
					</div>
				</div>
				
			`;
			container.appendChild(card);

			coursesContainer.appendChild(container);

			fetching.innerHTML = null

		}
	})
}

getAllCourses()
