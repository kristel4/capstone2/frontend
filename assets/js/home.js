
let token = localStorage.getItem('token');
let navbar = document.querySelector('#navbar');
let navItems = document.querySelector("#navSession")
let registerBtn = document.querySelector("#registerBtn")
let coursesBtn = document.querySelector('#coursesBtn');
let profileBtn = document.querySelector('#profileBtn');
let logOutBtn = document.querySelector('#registerBtn');
let isAdmin = localStorage.getItem('isAdmin');


if(!token) {

  navSession.innerHTML = 
  `
    <li class="nav-item"> 
      <a href="./pages/login.html" class="nav-link"> Log in </a>
    </li>
  `
  registerBtn.innerHTML = 
  `
    <li class="nav-item"> 
      <a href="./pages/register.html" class="nav-link"> Sign Up </a>
    </li>
  `

} else {
  console.log('logged in')
  let profileUrl = isAdmin === 'false' ? 'userProfile' : 'adminProfile';
  profileBtn.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./pages/${profileUrl}.html" class="nav-link"> Profile </a>
      </li>
    `

  coursesBtn.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./pages/courses.html" class="nav-link"> Courses </a>
      </li>
    ` 
  navSession.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./pages/logout.html" class="nav-link"> Logout </a>
      </li>
    ` 
}

let randomNumber = Math.floor(Math.random() * 20);

const getQuotes = () => {
fetch("https://type.fit/api/quotes")
  .then(function(response) {
    return response.json();
  })
  .then(function(data) {
    console.log(data[randomNumber]);
    let container = document.createElement('div');

    let quoteCard = document.createElement('div');

    quoteCard.innerHTML = `
    	<h3 class="m-3">Food for thought:</h3>
    	<p class="font-italic m-2"> "${data[randomNumber].text}" </p>
    	<p class="font-weight-bold">${data[randomNumber].author || "Anonymous"}</p>
    `;

    container.appendChild(quoteCard);
    quotes.appendChild(container);

  });
}

getQuotes()

//courses

fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {
  //log the data to check if you were able to fetch the data from the server
  console.log(data)
  let i;
  for(i = 0; i < data.length; i++) {
    let ul = document.querySelector('#splider')
    let card = document.createElement('li');
    card.className = 'splide__slide';

      card.innerHTML = `
          <div class="card">
            <div class="card-header">
              <h5 class="card-title text-center">${data[i].name}</h5>
            </div>
            <div class="card-body" style="height: 100px; border-bottom: 1px solid #b3abab;">
              <p class="card-text">${data[i].description}</p>
            </div
            <div class="card-footer">
              <p class="card-text text-center py-2">PHP ${data[i].price}</p>
            </div>
          </div>

      `;
      ul.appendChild(card);

  }

  new Splide( '#card-slider', {
    perPage    : 3,
    breakpoints: {
      600: {
        perPage: 1,
      }
    }
  } ).mount();

});
