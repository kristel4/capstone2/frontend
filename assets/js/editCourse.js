let token = localStorage.getItem('token');
const url_string = window.location.href; //window.location.href
const url = new URL(url_string);
const courseId = url.searchParams.get("courseId");
let name = document.querySelector('#courseName');
let price = document.querySelector('#coursePrice');
let description = document.querySelector('#courseDescription')
let editCourse = document.querySelector("#editCourse")


const getCourseDetails = () => {
//getting data from the db to display in input
fetch(`http://localhost:4000/api/courses/${courseId}`, {
		methods: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		name.value = data.name
		description.value = data.description
		price.value = data.price

})
}

editCourse.addEventListener('submit', (e) => {
	e.preventDefault()

	let courseName = document.querySelector('#courseName').value;
	let courseDescription = document.querySelector('#courseDescription').value;
	let coursePrice = document.querySelector('#coursePrice').value;

	fetch('http://localhost:4000/api/courses', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId,
			name: courseName,
			description: courseDescription,
			price: coursePrice
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//redirect to course
				alert('Course updated successfully!')
				window.location.replace("./courses.html")
			} else {
				//redirect in creating course
				alert("Something went wrong!")
			}
		})
})

getCourseDetails()