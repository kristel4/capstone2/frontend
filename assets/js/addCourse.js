let submitForm = document.querySelector('#createCourse')

submitForm.addEventListener('submit', (e) => {
	e.preventDefault()

	let courseName = document.querySelector('#courseName').value;
	let courseDescription = document.querySelector('#courseDescription').value;
	let coursePrice = document.querySelector('#coursePrice').value;

	let token = localStorage.getItem('token');

	fetch('http://localhost:4000/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: courseDescription,
			price: coursePrice
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				//redirect to course
				alert('Course successfully added!')
				window.location.replace("./courses.html")
			} else {
				//redirect in creating course
				alert("Something went wrong!")
			}
		})
})