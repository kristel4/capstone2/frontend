
let navbar = document.querySelector('#navbar');
let navItems = document.querySelector("#navSession")
let registerBtn = document.querySelector("#registerBtn")
let coursesBtn = document.querySelector('#coursesBtn');
let profileBtn = document.querySelector('#profileBtn');
let logOutBtn = document.querySelector('#registerBtn');
let isAdmin = localStorage.getItem('isAdmin');

if(!localStorage.getItem('token')) {

  navSession.innerHTML = 
  `
    <li class="nav-item"> 
      <a href="./login.html" class="nav-link"> Log In </a>
    </li>
  `
  registerBtn.innerHTML = 
  `
    <li class="nav-item"> 
      <a href="./register.html" class="nav-link"> Sign Up </a>
    </li>
  `

} else {


  let profileUrl = isAdmin === 'false' ? 'userProfile' : 'adminProfile';
  profileBtn.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./${profileUrl}.html" class="nav-link"> Profile </a>
      </li>
    `

  coursesBtn.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./courses.html" class="nav-link"> Courses </a>
      </li>
    ` 
  navSession.innerHTML = 
    `
      <li class="nav-item"> 
        <a href="./logout.html" class="nav-link"> Logout </a>
      </li>
    ` 
}