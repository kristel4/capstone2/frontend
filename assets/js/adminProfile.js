let baseUrl = 'http://localhost:4000/api'; //for shorter code of URLs
let avatarInput = document.querySelector('#avatarInput');
let profileImage = document.querySelector("#avatar");
let token = localStorage.getItem('token');
let userId = localStorage.getItem('id');
let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let email = document.querySelector('#email');
let mobile = document.querySelector("#mobileNumber");
let courseTable = document.querySelector("#courseTable");
let uploadBtn = document.querySelector('#uploadBtn');
let courseContainer = document.querySelector('#courseContainer');


avatarInput.addEventListener('change', (e) => {
	// e.preventDefault();
	let file = e.target.files[0];
	let reader = new FileReader();


	reader.onload = (e) => {
		profileImage.src = e.target.result
		console.log(e)
	}

	reader.readAsDataURL(file);
})

uploadBtn.addEventListener('click', (e) => {
	e.preventDefault();
	uploadImage(profileImage.src)
})


const uploadImage = (avatar) => {
	fetch(`${baseUrl}/users/upload`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			userId,
			avatar
		})		
	}).then( res => {
		//check if there is an error encountered
		if(res.status !== 200) {
			//if so return the error text
				return res.statusText
		} 
		return res.json()
	})
	.then(data => {
		console.log(data);

		if(data === true) {
			alert('Avatar successfully uploaded!');
		} 
		else {
			alert(data)
		} 
	})
}

const getUserProfile = () => {

	fetch(`${baseUrl}/users/details`, {
		methods: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data)
		firstName.innerHTML = data.firstName;
		lastName.innerHTML = data.lastName;
		email.innerHTML = data.email;
		mobile.innerHTML = data.mobileNo;
		profileImage.src = data.avatar;

		})
	
}


const getAllCourses = () => {
	fetch(`${baseUrl}/courses/`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`			
		}
	})
	.then( res => res.json())
	.then( data => {
		console.log(data)

		let i;

		for(i = 0; i < data.length; i++) {
			let container = document.createElement('div');


			
			container.className = 'col-3 my-3';

			let card = document.createElement('div');

			card.className = 'card p-3 courseEnroll'
			card.innerHTML = `
				
					<div>
					  <div >
					    <h5 class="card-title text-center">${data[i].name}</h5>
					    	<p>Students enrolled:</p>
					  </div>
					</div>

			`;


			for(x = 0; x < data[i].enrollees.length; x++) {
				let ol = document.createElement('ul');
				ol.className = "ml-4";

				let studentNameTr = document.createElement('li')
				studentNameTr.innerHTML = data[i].enrollees[x].fullName
				ol.appendChild(studentNameTr)
				card.appendChild(ol)
			}

			container.appendChild(card)
			courseContainer.appendChild(container);

			
		}
	})
}
getUserProfile()
getAllCourses()