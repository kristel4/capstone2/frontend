let adminUser = localStorage.getItem("isAdmin");

//cardfooter will be dynamically rendered if the use is an admin or not
let cardFooter;

//fetch request to all the available user
fetch('http://localhost:4000/api/courses')
.then(res => res.json())
.then(data => {
	//log the data to check if you were able to fetch the data from the server
	console.log(data)

	//courseData will store the data to be rendered
	let courseData

	if(data.length < 1) {
		courseData = "No courses available."
	} else {
		//else iterate the courses collection and display each course
		courseData = data.map(course => {

			//for non admin user course selection if the user is already enrolled the button will be disabled and label will change to 'already enrolled'

			const enrollees = course.enrollees

			//user this function (array.some) to check if logged in user id is in the enrollees id

			let isUserEnrolled = enrollees.some( val => val.userId === localStorage.getItem('id') );

			//if the user is regular user, display when the course was created
			if(adminUser === "false" || !adminUser) {
				cardFooter =
					`
						<a href="./course.html?courseId=${course._id}" value="{course._id"} class="btn text-white btn-block editButton ${isUserEnrolled && 'disabled' }"  >
						${isUserEnrolled ? 'Already Enrolled' : 'Select Course' }
						</a>
					`

			} else {
				//for admin user
				cardFooter = 
					`
						<a href="./editCourse.html?courseId=${course._id}" value="{course._id}" class="btn text-white btn-block editButton">Edit</a>
						<a href="./deleteCourse.html?courseId=${course._id}" value="{course._id}" class="btn text-white btn-block deleteButton">Delete</a>
					`
			}
			return(
					`
						<div class="col-md-6 my-3">
							<div class="card">
								<div class="card-body">
									<h5 class="card-title">
										${course.name}
									</h5>
									<p class="card-text text-left">
										${course.description}
									</p>
									<p class="card-text text-right">
										${course.price}
									</p>
								</div>
								<div class="card-footer">
									${cardFooter}
								</div>
							</div>
						</div>

					`
			)
		}).join("");
	}
	let container = document.querySelector("#coursesContainer")

	//get the value of courseData anmd assign it as the #courseContainer's content
	container.innerHTML = courseData;
})

//add modal - if user is an admin, there will a button to add a course
let modalButton = document.querySelector("#adminButton")

if(adminUser == "false" || !adminUser) {
	//if user is regular user, do not show add course button
	modalButton.innerHTML = null
} else {
	//display add course if user is an admin
	modalButton.innerHTML =
		`
			<div class="col-md-2 offset-md-10">
				<a href="./addCourse.html" class="btn btn-block mb-2">Add Course</a>
			</div>
		`
}

//add modal - if user is an admin, there will a button to add a course
let archiveButton = document.querySelector("#admin2Button")

if(adminUser == "false" || !adminUser) {
	//if user is regular user, do not show add course button
	archiveButton.innerHTML = null
} else {
	//display add course if user is an admin
	archiveButton.innerHTML =
		`
			<div class="col-md-2 offset-md-10">
				<a href="./archiveCourses.html" class="btn btn-block">Archive</a>
			</div>
		`
}
